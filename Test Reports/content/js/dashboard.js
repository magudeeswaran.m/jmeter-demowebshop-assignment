/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 100.0, "KoPercent": 0.0};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.6978260869565217, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.1, 500, 1500, "https://demowebshop.tricentis.com/-9"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/-8"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-23"], "isController": false}, {"data": [0.9, 500, 1500, "https://demowebshop.tricentis.com/logout-22"], "isController": false}, {"data": [0.9, 500, 1500, "https://demowebshop.tricentis.com/logout-21"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-20"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-18"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-19"], "isController": false}, {"data": [0.0, 500, 1500, "https://demowebshop.tricentis.com/login"], "isController": false}, {"data": [0.0, 500, 1500, "https://demowebshop.tricentis.com/logout"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-23"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-24"], "isController": false}, {"data": [0.0, 500, 1500, "Test"], "isController": true}, {"data": [0.9, 500, 1500, "https://demowebshop.tricentis.com/login-21"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-22"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-24"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-20"], "isController": false}, {"data": [0.75, 500, 1500, "https://demowebshop.tricentis.com/login-2"], "isController": false}, {"data": [0.65, 500, 1500, "https://demowebshop.tricentis.com/login-1"], "isController": false}, {"data": [0.8, 500, 1500, "https://demowebshop.tricentis.com/login-4"], "isController": false}, {"data": [0.8, 500, 1500, "https://demowebshop.tricentis.com/login-3"], "isController": false}, {"data": [0.85, 500, 1500, "https://demowebshop.tricentis.com/login-6"], "isController": false}, {"data": [0.7, 500, 1500, "https://demowebshop.tricentis.com/login-5"], "isController": false}, {"data": [0.55, 500, 1500, "https://demowebshop.tricentis.com/login-8"], "isController": false}, {"data": [0.65, 500, 1500, "https://demowebshop.tricentis.com/login-7"], "isController": false}, {"data": [0.75, 500, 1500, "https://demowebshop.tricentis.com/login-0"], "isController": false}, {"data": [0.0, 500, 1500, "https://demowebshop.tricentis.com/"], "isController": false}, {"data": [0.7, 500, 1500, "https://demowebshop.tricentis.com/-14"], "isController": false}, {"data": [0.7, 500, 1500, "https://demowebshop.tricentis.com/-13"], "isController": false}, {"data": [0.7, 500, 1500, "https://demowebshop.tricentis.com/-12"], "isController": false}, {"data": [0.8, 500, 1500, "https://demowebshop.tricentis.com/-11"], "isController": false}, {"data": [0.7, 500, 1500, "https://demowebshop.tricentis.com/-18"], "isController": false}, {"data": [0.0, 500, 1500, "https://demowebshop.tricentis.com/-17"], "isController": false}, {"data": [0.7, 500, 1500, "https://demowebshop.tricentis.com/-16"], "isController": false}, {"data": [0.7, 500, 1500, "https://demowebshop.tricentis.com/-15"], "isController": false}, {"data": [0.5, 500, 1500, "https://demowebshop.tricentis.com/-5"], "isController": false}, {"data": [0.5, 500, 1500, "https://demowebshop.tricentis.com/-4"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/-7"], "isController": false}, {"data": [0.0, 500, 1500, "https://demowebshop.tricentis.com/-6"], "isController": false}, {"data": [0.4, 500, 1500, "https://demowebshop.tricentis.com/-1"], "isController": false}, {"data": [0.8, 500, 1500, "https://demowebshop.tricentis.com/-10"], "isController": false}, {"data": [0.1, 500, 1500, "https://demowebshop.tricentis.com/-0"], "isController": false}, {"data": [0.1, 500, 1500, "https://demowebshop.tricentis.com/-3"], "isController": false}, {"data": [0.2, 500, 1500, "https://demowebshop.tricentis.com/-2"], "isController": false}, {"data": [0.9, 500, 1500, "https://demowebshop.tricentis.com/logout-5"], "isController": false}, {"data": [0.9, 500, 1500, "https://demowebshop.tricentis.com/logout-4"], "isController": false}, {"data": [0.8, 500, 1500, "https://demowebshop.tricentis.com/logout-3"], "isController": false}, {"data": [0.9, 500, 1500, "https://demowebshop.tricentis.com/logout-2"], "isController": false}, {"data": [0.5, 500, 1500, "https://demowebshop.tricentis.com/logout-1"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-0"], "isController": false}, {"data": [0.8, 500, 1500, "https://demowebshop.tricentis.com/-19"], "isController": false}, {"data": [0.8, 500, 1500, "https://demowebshop.tricentis.com/logout-9"], "isController": false}, {"data": [0.7, 500, 1500, "https://demowebshop.tricentis.com/logout-8"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-7"], "isController": false}, {"data": [0.9, 500, 1500, "https://demowebshop.tricentis.com/logout-6"], "isController": false}, {"data": [0.5, 500, 1500, "https://demowebshop.tricentis.com/-23"], "isController": false}, {"data": [0.5, 500, 1500, "https://demowebshop.tricentis.com/-22"], "isController": false}, {"data": [0.6, 500, 1500, "https://demowebshop.tricentis.com/login-9"], "isController": false}, {"data": [0.5, 500, 1500, "https://demowebshop.tricentis.com/-21"], "isController": false}, {"data": [0.6, 500, 1500, "https://demowebshop.tricentis.com/-20"], "isController": false}, {"data": [0.9, 500, 1500, "https://demowebshop.tricentis.com/logout-12"], "isController": false}, {"data": [0.7, 500, 1500, "https://demowebshop.tricentis.com/logout-11"], "isController": false}, {"data": [0.8, 500, 1500, "https://demowebshop.tricentis.com/logout-10"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-16"], "isController": false}, {"data": [0.9, 500, 1500, "https://demowebshop.tricentis.com/login-17"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-19"], "isController": false}, {"data": [0.9, 500, 1500, "https://demowebshop.tricentis.com/login-14"], "isController": false}, {"data": [0.9, 500, 1500, "https://demowebshop.tricentis.com/logout-18"], "isController": false}, {"data": [0.9, 500, 1500, "https://demowebshop.tricentis.com/login-15"], "isController": false}, {"data": [0.9, 500, 1500, "https://demowebshop.tricentis.com/logout-17"], "isController": false}, {"data": [0.6, 500, 1500, "https://demowebshop.tricentis.com/login-12"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-16"], "isController": false}, {"data": [0.5, 500, 1500, "https://demowebshop.tricentis.com/login-13"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-15"], "isController": false}, {"data": [0.65, 500, 1500, "https://demowebshop.tricentis.com/login-10"], "isController": false}, {"data": [0.9, 500, 1500, "https://demowebshop.tricentis.com/logout-14"], "isController": false}, {"data": [0.75, 500, 1500, "https://demowebshop.tricentis.com/login-11"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-13"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 455, 0, 0.0, 793.3912087912095, 218, 7073, 476.0, 1563.4000000000008, 2432.7999999999993, 6305.519999999999, 7.486384652088784, 195.51736650994619, 13.223450328660514], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["https://demowebshop.tricentis.com/-9", 5, 0, 0.0, 2214.8, 695, 3885, 2198.0, 3885.0, 3885.0, 3885.0, 1.2565971349585323, 280.3303770576778, 0.9264949579039959], "isController": false}, {"data": ["https://demowebshop.tricentis.com/-8", 5, 0, 0.0, 375.2, 313, 466, 372.0, 466.0, 466.0, 466.0, 5.518763796909493, 32.30417011589404, 4.090568087748344], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-23", 5, 0, 0.0, 253.2, 234, 303, 239.0, 303.0, 303.0, 303.0, 0.21129141311697092, 0.025586069557133197, 0.18941945043103448], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-22", 5, 0, 0.0, 404.4, 226, 998, 242.0, 998.0, 998.0, 998.0, 0.20517871065698223, 0.024845859493618942, 0.18193580984037097], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-21", 5, 0, 0.0, 418.6, 232, 984, 279.0, 984.0, 984.0, 984.0, 0.2052376652163205, 0.024652571114851, 0.18299022299072326], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-20", 5, 0, 0.0, 244.8, 222, 284, 238.0, 284.0, 284.0, 284.0, 0.21373915273799857, 0.026091205168212714, 0.18785667721113153], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-18", 5, 0, 0.0, 265.0, 221, 339, 261.0, 339.0, 339.0, 339.0, 0.2080039936766786, 0.040219522214826525, 0.2551298984940511], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-19", 5, 0, 0.0, 247.4, 233, 263, 245.0, 263.0, 263.0, 263.0, 0.20848100738022768, 0.02524574698744944, 0.26080485395905434], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login", 10, 0, 0.0, 2844.3, 1605, 4403, 2596.5, 4378.4, 4403.0, 4403.0, 0.24197256031166067, 7.493351425823312, 4.951576186572943], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout", 5, 0, 0.0, 2839.6, 2051, 3460, 2986.0, 3460.0, 3460.0, 3460.0, 0.1912631015224543, 7.265382932637136, 4.175662009410145], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-23", 5, 0, 0.0, 249.4, 234, 268, 254.0, 268.0, 268.0, 268.0, 0.21032263492197031, 0.02546875657258234, 0.2657787984267867], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-24", 5, 0, 0.0, 281.0, 229, 415, 247.0, 415.0, 415.0, 415.0, 0.21038458301775645, 0.025681711794159727, 0.26236436768913574], "isController": false}, {"data": ["Test", 5, 0, 0.0, 15156.0, 13664, 15741, 15408.0, 15741.0, 15741.0, 15741.0, 0.3028284174186906, 359.8497663489492, 24.337763933892557], "isController": true}, {"data": ["https://demowebshop.tricentis.com/login-21", 5, 0, 0.0, 354.8, 253, 744, 254.0, 744.0, 744.0, 744.0, 0.20922252908193154, 0.025131221754958574, 0.2633670312369236], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-22", 5, 0, 0.0, 250.8, 220, 267, 251.0, 267.0, 267.0, 267.0, 0.2087246921310791, 0.025275255687747862, 0.26172119599248594], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-24", 5, 0, 0.0, 261.6, 229, 293, 264.0, 293.0, 293.0, 293.0, 0.2114164904862579, 0.025807677061310784, 0.18602173625792812], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-20", 5, 0, 0.0, 261.2, 228, 282, 262.0, 282.0, 282.0, 282.0, 0.2092487968194183, 0.025543066017995397, 0.26074361791169703], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-2", 10, 0, 0.0, 641.4, 239, 1105, 632.5, 1096.3, 1105.0, 1105.0, 0.2536912070627632, 0.030720419605256483, 0.26062807600588567], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-1", 10, 0, 0.0, 597.3, 245, 970, 642.5, 967.4, 970.0, 970.0, 0.25561065385205256, 4.42163995801595, 0.24674915169214254], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-4", 10, 0, 0.0, 542.5999999999999, 241, 996, 273.0, 992.8, 996.0, 996.0, 0.253852207244942, 0.030492013174929555, 0.26240484508669054], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-3", 10, 0, 0.0, 552.8000000000001, 234, 1023, 264.5, 1022.0, 1023.0, 1023.0, 0.2539295599400726, 0.03062529360605368, 0.26434463954698967], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-6", 10, 0, 0.0, 503.4, 245, 1157, 266.5, 1144.7, 1157.0, 1157.0, 0.25396825396825395, 0.030629960317460316, 0.2650049603174603], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-5", 10, 0, 0.0, 708.8000000000001, 232, 1113, 964.0, 1109.1, 1113.0, 1113.0, 0.25405213149738326, 0.030764125298511254, 0.2623634072836746], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-8", 10, 0, 0.0, 893.3, 245, 1116, 949.5, 1115.0, 1116.0, 1116.0, 0.24947610018960184, 0.030209996507334594, 0.25447049477347566], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-7", 10, 0, 0.0, 767.4, 225, 1118, 934.5, 1108.9, 1118.0, 1118.0, 0.2556302563971472, 0.030830406899460622, 0.26074785430353536], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-0", 10, 0, 0.0, 594.6, 284, 1064, 472.0, 1063.5, 1064.0, 1064.0, 0.25750630890456816, 2.981943174795282, 0.19878782928619249], "isController": false}, {"data": ["https://demowebshop.tricentis.com/", 5, 0, 0.0, 6627.8, 6071, 7073, 6606.0, 7073.0, 7073.0, 7073.0, 0.6799020940984498, 739.9877776975115, 11.97265093826489], "isController": false}, {"data": ["https://demowebshop.tricentis.com/-14", 5, 0, 0.0, 483.6, 396, 546, 503.0, 546.0, 546.0, 546.0, 5.324813631522897, 7.446419062832801, 3.7856096911608095], "isController": false}, {"data": ["https://demowebshop.tricentis.com/-13", 5, 0, 0.0, 576.0, 432, 959, 503.0, 959.0, 959.0, 959.0, 4.299226139294927, 125.21915976999139, 3.245411919604471], "isController": false}, {"data": ["https://demowebshop.tricentis.com/-12", 5, 0, 0.0, 878.6, 294, 2609, 499.0, 2609.0, 2609.0, 2609.0, 1.787629603146228, 6.75598297282803, 1.2970789015016089], "isController": false}, {"data": ["https://demowebshop.tricentis.com/-11", 5, 0, 0.0, 832.5999999999999, 339, 2612, 388.0, 2612.0, 2612.0, 2612.0, 1.7041581458759374, 5.6833008478186775, 1.2331847520449897], "isController": false}, {"data": ["https://demowebshop.tricentis.com/-18", 5, 0, 0.0, 586.8, 451, 777, 534.0, 777.0, 777.0, 777.0, 3.855050115651504, 11.542562162683115, 2.921405165767155], "isController": false}, {"data": ["https://demowebshop.tricentis.com/-17", 5, 0, 0.0, 2675.4, 2393, 2984, 2662.0, 2984.0, 2984.0, 2984.0, 1.3892747985551543, 597.9674801160044, 1.0175352528480133], "isController": false}, {"data": ["https://demowebshop.tricentis.com/-16", 5, 0, 0.0, 536.8, 466, 718, 503.0, 718.0, 718.0, 718.0, 3.7397157816005984, 203.60634232423337, 2.7390496447270007], "isController": false}, {"data": ["https://demowebshop.tricentis.com/-15", 5, 0, 0.0, 508.6, 495, 538, 505.0, 538.0, 538.0, 538.0, 4.646840148698885, 33.88018413104089, 3.4488266728624533], "isController": false}, {"data": ["https://demowebshop.tricentis.com/-5", 5, 0, 0.0, 1344.8, 1272, 1460, 1316.0, 1460.0, 1460.0, 1460.0, 2.9120559114735003, 6.7966929965055325, 2.24376183022714], "isController": false}, {"data": ["https://demowebshop.tricentis.com/-4", 5, 0, 0.0, 1321.6, 1241, 1458, 1303.0, 1458.0, 1458.0, 1458.0, 2.923976608187134, 6.727430555555555, 2.221536915204678], "isController": false}, {"data": ["https://demowebshop.tricentis.com/-7", 5, 0, 0.0, 378.8, 320, 415, 390.0, 415.0, 415.0, 415.0, 5.63063063063063, 121.92184860641892, 4.107501055743243], "isController": false}, {"data": ["https://demowebshop.tricentis.com/-6", 5, 0, 0.0, 1996.0, 1811, 2218, 1986.0, 2218.0, 2218.0, 2218.0, 2.0242914979757085, 186.72902960526315, 1.4727511386639676], "isController": false}, {"data": ["https://demowebshop.tricentis.com/-1", 5, 0, 0.0, 904.6, 611, 1924, 668.0, 1924.0, 1924.0, 1924.0, 2.598752598752599, 238.02188230899168, 1.9160724727130978], "isController": false}, {"data": ["https://demowebshop.tricentis.com/-10", 5, 0, 0.0, 679.0, 292, 1855, 424.0, 1855.0, 1855.0, 1855.0, 2.6954177897574128, 19.72866829514825, 1.9794474393530999], "isController": false}, {"data": ["https://demowebshop.tricentis.com/-0", 5, 0, 0.0, 1688.6, 1439, 2178, 1555.0, 2178.0, 2178.0, 2178.0, 2.286236854138089, 79.4824531321445, 1.1029306698673984], "isController": false}, {"data": ["https://demowebshop.tricentis.com/-3", 5, 0, 0.0, 1603.2, 1495, 1684, 1580.0, 1684.0, 1684.0, 1684.0, 2.57201646090535, 68.56533725565843, 1.9692001028806585], "isController": false}, {"data": ["https://demowebshop.tricentis.com/-2", 5, 0, 0.0, 1329.0, 292, 1742, 1537.0, 1742.0, 1742.0, 1742.0, 2.4987506246876565, 62.31259370314843, 1.8521012931034484], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-5", 5, 0, 0.0, 396.0, 236, 958, 263.0, 958.0, 958.0, 958.0, 0.21734405564007825, 0.026319006737665726, 0.19251079928276463], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-4", 5, 0, 0.0, 431.6, 230, 931, 268.0, 931.0, 931.0, 931.0, 0.2177131411652007, 0.026151090198554383, 0.19390076635025688], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-3", 5, 0, 0.0, 455.4, 221, 943, 300.0, 943.0, 943.0, 943.0, 0.21728738429446787, 0.026312144191908218, 0.18842890356785885], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-2", 5, 0, 0.0, 331.6, 251, 504, 269.0, 504.0, 504.0, 504.0, 0.2175237100843992, 0.026340761768032715, 0.18778414034629773], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-1", 5, 0, 0.0, 734.4, 647, 845, 701.0, 845.0, 845.0, 845.0, 0.21351097446408746, 7.357371332949014, 0.158673683171065], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-0", 5, 0, 0.0, 299.4, 260, 365, 282.0, 365.0, 365.0, 365.0, 0.21794089442943074, 0.1470675371589225, 0.2328391977595676], "isController": false}, {"data": ["https://demowebshop.tricentis.com/-19", 5, 0, 0.0, 576.6, 476, 812, 491.0, 812.0, 812.0, 812.0, 3.4965034965034967, 26.005244755244757, 2.629206730769231], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-9", 5, 0, 0.0, 570.6, 241, 1099, 323.0, 1099.0, 1099.0, 1099.0, 0.20963481615026622, 0.02518074451804956, 0.18158797063016227], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-8", 5, 0, 0.0, 663.8, 233, 965, 872.0, 965.0, 965.0, 965.0, 0.21694797587538509, 0.026271043953659912, 0.18559221373714582], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-7", 5, 0, 0.0, 268.8, 251, 301, 256.0, 301.0, 301.0, 301.0, 0.2175237100843992, 0.026128336269903418, 0.1854474598668755], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-6", 5, 0, 0.0, 446.0, 242, 1134, 288.0, 1134.0, 1134.0, 1134.0, 0.2171646977067408, 0.026297287612925643, 0.19468475829569146], "isController": false}, {"data": ["https://demowebshop.tricentis.com/-23", 5, 0, 0.0, 964.0, 610, 1114, 1034.0, 1114.0, 1114.0, 1114.0, 2.4521824423737124, 9.43754980995586, 1.8463209600294261], "isController": false}, {"data": ["https://demowebshop.tricentis.com/-22", 5, 0, 0.0, 960.6, 620, 1061, 1049.0, 1061.0, 1061.0, 1061.0, 2.213368747233289, 8.516282093846835, 1.705417911686587], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-9", 10, 0, 0.0, 854.6999999999999, 231, 1134, 935.5, 1130.9, 1134.0, 1134.0, 0.249806400039969, 0.030128017973570485, 0.2548074070720192], "isController": false}, {"data": ["https://demowebshop.tricentis.com/-21", 5, 0, 0.0, 811.2, 504, 1118, 733.0, 1118.0, 1118.0, 1118.0, 2.5484199796126403, 9.842774432976555, 1.9386905899592253], "isController": false}, {"data": ["https://demowebshop.tricentis.com/-20", 5, 0, 0.0, 690.6, 473, 922, 691.0, 922.0, 922.0, 922.0, 2.736726874657909, 10.532656848659004, 2.0979790982484947], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-12", 5, 0, 0.0, 397.0, 233, 915, 267.0, 915.0, 915.0, 915.0, 0.217296827466319, 0.026313287700999565, 0.1846174217731421], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-11", 5, 0, 0.0, 688.4, 228, 1080, 889.0, 1080.0, 1080.0, 1080.0, 0.209881207236704, 0.025415302438819627, 0.18057162458548462], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-10", 5, 0, 0.0, 576.0, 241, 1099, 314.0, 1099.0, 1099.0, 1099.0, 0.20969635967119613, 0.025392918553933903, 0.18102693549739976], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-16", 5, 0, 0.0, 270.2, 254, 309, 263.0, 309.0, 309.0, 309.0, 0.20671407309409626, 0.02462804386472631, 0.2549608147635191], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-17", 5, 0, 0.0, 322.0, 248, 568, 263.0, 568.0, 568.0, 568.0, 0.2079002079002079, 0.0251754158004158, 0.2547995712058212], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-19", 5, 0, 0.0, 256.4, 225, 295, 250.0, 295.0, 295.0, 295.0, 0.21219708865594364, 0.025695741204430675, 0.18753746604846583], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-14", 5, 0, 0.0, 409.6, 245, 967, 286.0, 967.0, 967.0, 967.0, 0.20721952836835345, 0.0250929897633553, 0.25861968481909736], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-18", 5, 0, 0.0, 387.8, 223, 946, 265.0, 946.0, 946.0, 946.0, 0.2141877998629198, 0.04141521911411926, 0.1840676405071967], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-15", 5, 0, 0.0, 427.59999999999997, 235, 1109, 260.0, 1109.0, 1109.0, 1109.0, 0.2069450767766235, 0.024857660589379577, 0.24898079549687513], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-17", 5, 0, 0.0, 409.59999999999997, 229, 1124, 231.0, 1124.0, 1124.0, 1124.0, 0.2146014850422765, 0.02598689857933817, 0.18421357944546976], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-12", 10, 0, 0.0, 804.3, 285, 1076, 973.0, 1072.4, 1076.0, 1076.0, 0.25194628505202693, 0.030263079161522762, 0.25699013157894735], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-16", 5, 0, 0.0, 248.2, 218, 305, 235.0, 305.0, 305.0, 305.0, 0.21645958699510803, 0.02578913048183904, 0.1874996617818953], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-13", 5, 0, 0.0, 974.8, 947, 1010, 967.0, 1010.0, 1010.0, 1010.0, 0.20815120103242996, 0.025205809500020816, 0.253684276258274], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-15", 5, 0, 0.0, 248.8, 228, 291, 235.0, 291.0, 291.0, 291.0, 0.2166190104843601, 0.02601966629841435, 0.18107995407676977], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-10", 10, 0, 0.0, 769.6, 232, 1132, 926.5, 1127.8, 1132.0, 1132.0, 0.24875003109375388, 0.03012207407775926, 0.2536084301385538], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-14", 5, 0, 0.0, 424.4, 228, 1122, 254.0, 1122.0, 1122.0, 1122.0, 0.2088903743315508, 0.02529531876671123, 0.18400304457720587], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-11", 10, 0, 0.0, 637.6, 224, 1139, 655.5, 1131.3, 1139.0, 1139.0, 0.24894199651481205, 0.03002376618122977, 0.25149462440876275], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-13", 5, 0, 0.0, 259.4, 240, 301, 255.0, 301.0, 301.0, 301.0, 0.2175331738090059, 0.026341907765934307, 0.18524309332173156], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": []}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 455, 0, "", "", "", "", "", "", "", "", "", ""], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
